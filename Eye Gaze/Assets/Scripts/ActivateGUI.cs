﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateGUI : MonoBehaviour
{
    [SerializeField] private GameObject canvasUi;
    [SerializeField] private GameObject recipeManager;
    [SerializeField] private GameObject audioSource;
    [SerializeField] private GameObject audioSourceButton;

    /**
     * <summary>Activates or Deactivates the GUI based on State input</summary>
     * <param name="state">bool value used to Activate or Deactivate GUI</param>
     */
    public void SetActive(bool state)
    {
        canvasUi.SetActive(state);   
        recipeManager.SetActive(state);
        audioSource.SetActive(state);
        audioSourceButton.SetActive(state);
    }
}
