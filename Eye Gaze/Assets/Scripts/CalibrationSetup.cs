﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Profiling;
using UnityEngine.Serialization;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;

public class CalibrationSetup : MonoBehaviour
{
    [FormerlySerializedAs("calibrationIcon")] [SerializeField] private GameObject calibrationPrefab;
    [FormerlySerializedAs("centerFaceIcon")] [SerializeField] private GameObject centerFacePrefab;
    
    private ARFace _arFace;
    private GameObject _calibrationInstance;
    
    private Rect _size;
    private Vector2[] _eyeValueAtLocation;
    private Vector2[] _calibrationLocation;

    private bool _isCalibrated;
    
    private float _mid;
    private float _max;
    private float _min;
    private float _calibrationImageSize;
    
    private int _location;
    

    private void Awake()
    {
        _location = 0;
        _eyeValueAtLocation = new Vector2[5];
        _calibrationLocation = new Vector2[5];
        _size = gameObject.GetComponent<RectTransform>().rect;
        _calibrationImageSize = calibrationPrefab.GetComponent<RectTransform>().rect.width;
    }

    private void Start()
    {
        _calibrationLocation[0] = new Vector2(_size.width / 2, _size.height / 2);
        _calibrationLocation[1] = new Vector2(_size.width / 2, _size.height - 150);
        _calibrationLocation[2] = new Vector2(_size.width / 2, _calibrationImageSize);
        _calibrationLocation[3] = new Vector2(_calibrationImageSize, _size.height / 2);
        _calibrationLocation[4] = new Vector2(_size.width - _calibrationImageSize, _size.height / 2);
    }

    /**
     * <summary>Begin Calibrating the rotational values of the eyes at specific locations</summary>
     * <param name="arFace">The ARFace attached to the program so that we may access its 'eye' values</param>
     */
    public void StartCalibration(ARFace arFace)
    {
        // if we have already calibrated before just exit the method
        if (_isCalibrated) return;
        
        _arFace = arFace;
        
        /* we call the setup method through a coroutine so that we can pause for a few
         seconds before calibration */
        StartCoroutine(Setup());
    }

    private IEnumerator Setup()
    {
        _isCalibrated = true;
        
        var centerFacePosition = new Vector3(_calibrationLocation[0].x, _calibrationLocation[0].y, 0);
        var centerFaceGraphic = Instantiate(centerFacePrefab, centerFacePosition, Quaternion.identity);
        centerFaceGraphic.transform.SetParent(transform, true);
        
        yield return new WaitForSeconds(3);
        
        var faceSavedPosition = CenterFace();
        Destroy(centerFaceGraphic);
        
        while (_location <= 4)
        {
            var position = new Vector3(_calibrationLocation[_location].x, _calibrationLocation[_location].y, 0);
            
            // spawn the calibration icon and set the parent so it is visible
            _calibrationInstance = Instantiate(calibrationPrefab, position, Quaternion.identity);
            _calibrationInstance.transform.SetParent(transform, true);

            //isCalibrating.text = " Calibration: TRUE " + _location;
            
            yield return new WaitForSeconds(3);
            // take the sum of 100 rotation values so that we can get an avg value
            for (var i = 0; i < 100; i++)
            {
                var rotation = _arFace.leftEye.rotation;
                _eyeValueAtLocation[_location].x += rotation.x;
                _eyeValueAtLocation[_location].y += rotation.y;
            }

            _eyeValueAtLocation[_location].x /= 100;
            _eyeValueAtLocation[_location].y /= 100;

            // update location value and destroy the current calibration prefab
            _location++;
            Destroy(_calibrationInstance);
        }

        // call startmoving so we can setup the cursor prefab
        GetComponent<Recenter>().SetArFaceAndFacePosition(_arFace, faceSavedPosition);
        GetComponent<Movement>().StartMoving(_eyeValueAtLocation, _arFace);
    }

    private Vector3 CenterFace()
    {
        var xDistance = 1f;
        var yDistance = 1f;
        var savedPosition = _arFace.transform.position;
        var startTime = Time.time;
        do
        {
            var currentPosition = _arFace.transform.position;
            xDistance = Vector3.Distance(new Vector3(currentPosition.x, 0,0), new Vector3(savedPosition.x, 0, 0));
            yDistance = Vector3.Distance(new Vector3(0, currentPosition.y,0), new Vector3(0, savedPosition.y, 0));
        } while (xDistance > 0.00100f && yDistance > 0.00100f);

        return savedPosition;
    }

}
