﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class Movement : MonoBehaviour
{
    [SerializeField] private GameObject cursorPrefab;
    [SerializeField] private GameObject recenterPrefab;
    [SerializeField] private GameObject hasRecenteredPrefab;
    
    private ARFace _arFace;
    private GameObject _cursor;
    
    private ActivateGUI _activateGui;
    private StaringToClicking _staringToClicking; 
    
    private Rect _size;
    private Vector2 _screenMid;
    private Vector3 _savedCenteredPosition;
    private Vector2[] _eyeValueAtLocation;
    
    
    private bool _isReady;
    
    private float _mid;
    private float _max;
    private float _min;
    private float _currentDesiredDistance;
    
    private float _screenMax;
    private float _screenMin;
    private float _screenLeft;
    private float _screenRight;
    private float _pixelsPerEyeMovementVertical;
    private float _pixelsPerEyeMovementHorizontal;
    
    private void Awake()
    {
        _activateGui = GetComponent<ActivateGUI>();
        _size = GetComponent<RectTransform>().rect;
        _currentDesiredDistance = 25f;
        
        _screenMin = 50;
        _screenLeft = 75;
        _screenRight = _size.width - 75;
        _screenMax = _size.height - 150;
        _screenMid = new Vector2(_size.width / 2, _size.height / 2);
    }
    
    /**
     * <summary>Activates the cursor and begins actively tracking the user's eyes</summary>
     * <param name="eyeValueAtLocation">The array holding the Rotational values of the eyes</param>
     * <param name="arFace">The ARFace component attached to the program needed to track the most up to date eye data</param>
     */
    public void StartMoving(Vector2[] eyeValueAtLocation, ARFace arFace)
    {
        _arFace = arFace;
        _eyeValueAtLocation = eyeValueAtLocation;
        
        var position = new Vector3(_screenMid.x, _screenMid.y, 0);
        _cursor = Instantiate(cursorPrefab, position, Quaternion.identity);
        _cursor.transform.SetParent(transform, true);
       // isMoving.text = "IsMoving: True";
        
        FindDistances();
        
        _staringToClicking = _cursor.GetComponent<StaringToClicking>();
        _staringToClicking.SetIsReady(true);
        
        _activateGui.SetActive(true);
        _isReady = true;
    }
    
    /**
     * <summary>Takes the rotational changes in the eyes and uses them to find how many pixels to move per rotational value</summary>
     */
    private void FindDistances()
    {
        var distanceFromMidToMaxScreenSpace = (_size.height - 150) - (_size.height / 2);
        var distanceFromMidToMaxWorldSpace = _eyeValueAtLocation[1].x - _eyeValueAtLocation[0].x;
        _pixelsPerEyeMovementVertical =  distanceFromMidToMaxWorldSpace / distanceFromMidToMaxScreenSpace;
        
        var distanceFromMidToLeftScreenSpace = 75 - (_size.width / 2);
        var distanceFromMidToLeftWorldSpace =  _eyeValueAtLocation[3].y - _eyeValueAtLocation[0].y;
        _pixelsPerEyeMovementHorizontal = distanceFromMidToLeftWorldSpace / distanceFromMidToLeftScreenSpace;
    }
    
    /**
     * <summary>Checks if the new location for the cursor is far enough away to justify moving the cursor</summary>
     * <param name="distance">The distance between the the new vector location and the current</param>
     */
    private bool IsDistanceGreatEnough(float distance)
    {

        if (distance > _currentDesiredDistance)
        {
            _currentDesiredDistance = 15f;
            return true;
        }
        else
        {
            _currentDesiredDistance = 225f;
            return false;
        }
    }

    // Update is called once per frame
    void Update()
    { 
        if (!_isReady) return;
        //Move();
        var leftEyeRotation = _arFace.leftEye.transform.rotation;
        var verticalValue = (leftEyeRotation.x -_eyeValueAtLocation[0].x) / _pixelsPerEyeMovementVertical;
        var horizontalValue = (leftEyeRotation.y - _eyeValueAtLocation[0].y) / _pixelsPerEyeMovementHorizontal;

        verticalValue += _screenMid.y;
        horizontalValue += _screenMid.x;

        if (verticalValue > _screenMax)
            verticalValue = _screenMax;
        else if (verticalValue < _screenMin)
            verticalValue = _screenMin;

        if (horizontalValue < _screenLeft)
            horizontalValue = _screenLeft;
        else if (horizontalValue > _screenRight)
            horizontalValue = _screenRight;
        
        var newPosition = new Vector3(horizontalValue, verticalValue, 0);
        var currentPosition = _cursor.transform.position;
        var distance = Vector3.Distance(currentPosition, newPosition);
        if (IsDistanceGreatEnough(distance))
        {
            _cursor.transform.position = Vector3.MoveTowards(currentPosition, newPosition, 720f * Time.deltaTime);
            
        }

        //isMoving.text = "IsMoving: True " + (horizontalValue) + "\n Distance: " + distance;
    }
}
