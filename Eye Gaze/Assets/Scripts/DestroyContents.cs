﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyContents : MonoBehaviour
{
   public void Destroy()
   {
      var childCount = transform.childCount - 1;
      for(var i = childCount; i >= 0; i--)
      {
         Destroy(gameObject.transform.GetChild(i).gameObject);
      }
   }
}
