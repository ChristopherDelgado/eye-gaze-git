﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class Recenter : MonoBehaviour
{
    [SerializeField] private GameObject recenterPrefab;
    [SerializeField] private GameObject hasRecenteredPrefab;

    private ARFace _arFace;
    private GameObject _recenterInstance;
    private GameObject _hasRecenteredInstance;

    private ActivateGUI _activateGui;
    
    private Rect _size;
    private Vector3 _savedPosition;
    
    private bool _isInstantiated;
    private bool _isRecentering;
    private bool _hasPassedCheck;
    private bool _isReady;

    private void Awake()
    {
        _isReady = false;
        _isRecentering = false;
        _isInstantiated = false;
        _hasPassedCheck = false;
        _activateGui = GetComponent<ActivateGUI>();
        _size = GetComponent<RectTransform>().rect;
    }
    
    public void SetArFaceAndFacePosition(ARFace arFace, Vector3 savedPosition)
    {
        _isReady = true;
        _arFace = arFace;
        _savedPosition = savedPosition;
    }
    
    /**
     * <summary>Assist the user in recentering their face</summary>
     */
    private void RecenterFace()
    {
        var xDistance = 1f;
        var yDistance = 1f;
        var startTime = 0f;
        
        if (!_isInstantiated)
        {
            _activateGui.SetActive(false);
            var position = new Vector3(_size.width / 2, _size.height / 2, 0);
            _recenterInstance = Instantiate(recenterPrefab, position, Quaternion.identity);
            _hasRecenteredInstance = Instantiate(hasRecenteredPrefab, position, Quaternion.identity);
            _recenterInstance.transform.SetParent(transform, true);
            _isInstantiated = true;
            startTime = Time.time;
        }

        if (Time.time - startTime > 5f && !_hasPassedCheck)
        {
            var currentPosition = _arFace.transform.position;
            xDistance = Vector3.Distance(new Vector3(currentPosition.x, 0, 0),
                new Vector3(_savedPosition.x, 0, 0));
            yDistance = Vector3.Distance(new Vector3(0, currentPosition.y, 0),
                new Vector3(0, _savedPosition.y, 0));
        }

        if ((xDistance <= 0.00100f && yDistance <= 0.00100f) || _hasPassedCheck)
        {
            if (!_hasPassedCheck)
            {
                Destroy(_recenterInstance);
                _hasRecenteredInstance.transform.SetParent(transform, true);
                startTime = Time.time;
                _hasPassedCheck = true;
            }

            if (Time.time - startTime > 5f)
            {
                Destroy(_hasRecenteredInstance);
                _isInstantiated = false;
                _isRecentering = false;
                _hasPassedCheck = false;
                _activateGui.SetActive(true);
            }
        }
    }
    
    // Update is called once per frame
    void Update()
    {
        if (!_isReady) return;
        if (_arFace.trackingState == TrackingState.None || _isRecentering)
        {
            _isRecentering = true;
            RecenterFace();
        }
    }
}
