﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

public class Hash
{
    /// <summary>
    /// Creates a hash for the given input using SHA256 Encryption and returns it in the form of a string
    /// </summary>
    /// <param name="myInput">The input you want hashed</param>
    /// <returns>The hashed input</returns>
    public static string ReturnHashedInput(string myInput)
    {
        // Creating the hash
        HashAlgorithm hashcode = SHA256.Create();
        var hashBytes = hashcode.ComputeHash(Encoding.UTF8.GetBytes(myInput));
        // forming the hash into a string
        var hashedInput = new StringBuilder();
        foreach (var Byte in hashBytes)
        {
            // append the given hashed byte to the hashed password in the form of a two digit hexadecimal
            hashedInput.Append(Byte.ToString("X2"));
        }

        return hashedInput.ToString();
    }
}
