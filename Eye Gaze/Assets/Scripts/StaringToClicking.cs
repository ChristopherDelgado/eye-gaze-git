﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;
using Button = UnityEngine.UI.Button;

public class StaringToClicking : MonoBehaviour
{
    private Button _button;
    
    private bool _isReady;
    private bool _isOnButton;

    private float _startTime;

    private int _buttonsHighlighted;
    
    private void Awake()
    {
        _startTime = 0.0f;
        _buttonsHighlighted = 0;
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Button")) return;
        _startTime = 0.0f;
        _button = other.gameObject.GetComponent<Button>();
        _buttonsHighlighted++;
        _isOnButton = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (--_buttonsHighlighted != 0)
        {
            _startTime = Time.time;
            return;
        }
        _isOnButton = false;
        _button = null;
    }

    
    public void SetIsReady(bool isReady)
    {
        _isReady = isReady;
    }
    
    /**
     * <summary>Click the button the cursor is hovering over after hovering over it for a set amount of time</summary>
     */
    private void Click()
    {
        if (_startTime <= 0.0f)
            _startTime = Time.time;
        if (Time.time - _startTime > 2f)
        {
            // click code goes in here, button.onClick.invoke()
            _button.onClick.Invoke();
            _startTime = 0.0f;
            _isOnButton = false;
            _button = null;
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (!_isReady) return;
        if (_isOnButton) Click();
    }
}
