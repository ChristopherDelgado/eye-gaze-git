﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StaringToToggling : MonoBehaviour
{
    private Toggle _toggle;
    
    private bool _isReady;
    private bool _isOnToggle;

    private float _startTime;

    private int _togglesHighlighted;
    
    private void Awake()
    {
        _startTime = 0.0f;
        _togglesHighlighted = 0;
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Toggle")) return;
        _startTime = 0.0f;
        _toggle = other.gameObject.GetComponent<Toggle>();
        _togglesHighlighted++;
        _isOnToggle = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (--_togglesHighlighted != 0)
        {
            _startTime = Time.time;
            return;
        }
        _isOnToggle = false;
        _toggle = null;
    }

    
    public void SetIsReady(bool isReady)
    {
        _isReady = isReady;
    }
    
    /**
     * <summary>Click the Toggle the cursor is hovering over after hovering over it for a set amount of time</summary>
     */
    private void Toggle()
    {
        if (_startTime <= 0.0f)
            _startTime = Time.time;
        if (Time.time - _startTime > 2f)
        {
            // click code goes in here, Toggle.onClick.invoke()
            _toggle.isOn = !_toggle.isOn;
            _toggle.onValueChanged.Invoke(_toggle.isOn);
            _startTime = 0.0f;
            _isOnToggle = false;
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (!_isReady) return;
        if (_isOnToggle) Toggle();
    }
}
