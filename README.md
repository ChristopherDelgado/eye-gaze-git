To install on any iPhone X or later here are the steps

STEP 1:
    Clone the repository, afterwards using the latest version of Unity Hub use the add button to add the Project folder if you see any errors in    the console area go to the Windows tab inside of Unity and open the package manager view, your in project plugins and make sure they are ALL up to date , IF the package manager will not open restart Unity.

STEP 2:
    From there in the Project window inside of Unity, open the Scenes folder and open the EyeTracking scene, afterwards you can go to the File tab and click Build settings, ensure that it is set to iOS, if not go ahead and do it now, then ensure then ensure that the EyeTracking scene is check marked inside of the scenes in build window, from here you can now press Build and choose the location on your drive to build to.

STEP 3:
    Now you will need access to a mac, move the build folder to a mac if it is not already on one, open a terminal inside of the build folder and run the command 'chmod -x MapFileParser.sh' this fixes a bug in which Unity does not make the file executable, you can then open the            unity-iphone.xcodeproj file and once it opens in XCode click the Unity_iPhone project file at the top of the hierarchy window and go to signing and capabilities, enable Automatically manage signing and then inside of Team add your personal team name, I do not believe you need an apple developer account as of XCode 7 but if it requires it you can make one but not pay the fee and it will work, then simply plugin and build to your device.
